from django.conf.urls import patterns, include, url
from django.contrib import admin
# from funny_app.views import *

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'funny_app.views.index'),

    # API
    url(r'^user_login/', 'funny_app.views.user_login'),
    url(r'^user_rate_completed_task/', 'funny_app.views.user_rate_completed_task'),
    url(r'^user_complete_task/', 'funny_app.views.user_complete_task'),

    url(r'^get_main_tasks/', 'funny_app.views.get_main_tasks'),
    url(r'^get_users_feed/', 'funny_app.views.get_users_feed'),
    url(r'^get_completed_task_feed/', 'funny_app.views.get_completed_task_feed'),


    url(r'^admin/', include(admin.site.urls)),
)
