import urllib
from django.http.response import JsonResponse
from django.shortcuts import render
from django.http import HttpResponse
from funny_app.models import *
from django.http import HttpResponse
from django import template
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from funny_app.models import *
from urllib.request import urlopen
import json
import operator


# Внутренниие API ф-и.

@csrf_exempt
def get_completed_task_feed(request):
    # Ф-я возвращает...
    if (request.method == 'POST'):
        # Проверяем делал ли пользователь это задание.
        vk_user_id = int(request.POST['vk_user_id'])
        task_id = int(request.POST['task_id'])

        user = User2.objects.get(vk_id=vk_user_id)
        clicked_task = Task.objects.get(id=task_id)

        # Загружаем выполненное задание из БД.
        len_tasks = CompletedTask.objects.filter(task_origin=clicked_task, user=user).count()
        if (len_tasks == 0):
            # Пользователь не делал этого задания (нет в БД).
            return JsonResponse({'result': False, 'task_desc':clicked_task.descr_full})
        else:
            # Такой объект есть - значит пользователь сделал это задание.
            # Покажем ему фид выполненных заданий.

            # Фильтруем таблицу выполненных заданий по главному заданию.
            cts = CompletedTask.objects.filter(task_origin=clicked_task).select_related('user')
            # Сортируем по рейтингу.
            cts = sorted(cts, key=operator.methodcaller('get_rating'), reverse=True)

            arr = []
            for ct in cts:
                u = ct.user
                s = {'image': ct.image, 'id':ct.id , 'user_comment': ct.user_comment, 'rating': ct.get_rating(),
                     'user_image': u.image, 'user_name': u.name, 'user_vk_id': u.vk_id}
                arr.append(s)
            return JsonResponse({'result': True, 'arr': arr})
    return JsonResponse({'error': True})

@csrf_exempt
def get_main_tasks(request):
    # Ф-я возвращает задания для главной страницы. Отсортированы по дате добавления (id).
    if (request.method == 'POST'):
        tasks = Task.objects.all().order_by('-id').values()
        return JsonResponse({'result': list(tasks)})
    return JsonResponse({'error': True})

@csrf_exempt
def get_users_feed(request):
    # Ф-я возвращает всех пользователей для страницы рейтинга. Отсортированы по рейтингу.
    if (request.method == 'POST'):
        # Сортируем по рейтингу.
        users = User2.objects.all()
        users = sorted(users, key=operator.methodcaller('get_rating'), reverse=True)
        # Сформировать ответ из отсортированного массива.
        arr = []
        for u in users:
            s = {'vk_id': u.vk_id, 'image': u.image, 'name': u.name, 'raiting': u.get_rating()}
            arr.append(s)
        return JsonResponse({'hh': arr})
    return JsonResponse({'error': True})



@csrf_exempt
def user_login(request):
    # Ф-я принимает id пользователя и сохраняет его в БД.
    if (request.method == 'POST'):
        vk_user_id = int(request.POST['vk_user_id'])
        # Смотрим зареген ли у нас этот пользователь.
        try:
            user = User2.objects.get(vk_id=vk_user_id)
            # Зареген.
            # return HttpResponse('Hello, mr. ' + user.name + '. You are already registered.')
            return JsonResponse({'registration': False})
        except User2.DoesNotExist:
            # Не зареган: подгружаем данные и созхраняем их.
            # Подгружаем данные о пользователе из vk-API: Имя, url photo.
            url = 'https://api.vk.com/method/users.get?fields=photo_50&user_id=' + str(vk_user_id)
            response = urllib.request.urlopen(url)
            codec = response.info().get_param('charset', 'utf8')  # default JSON encoding
            response = json.loads(response.read().decode(codec))
            name = response['response'][0]['first_name'] + ' ' + response['response'][0]['last_name']
            photo_url = response['response'][0]['photo_50']
            # Сохраняем пользователя.
            user = User2(name=name, vk_id=vk_user_id, image=photo_url)
            user.save()
            # return HttpResponse('User ' + user.name + ' registered.')
            return JsonResponse({'registration': True})
    return JsonResponse({'error': True})

@csrf_exempt
def user_rate_completed_task(request):
    # Ф-я принимает id пользователя, выполненной задачи и рейтинг. Записывая это в БД.
    if (request.method == 'POST'):
        try:
            vk_user_id = int(request.POST['vk_user_id'])
            completed_task_id = int(request.POST['completed_task_id'])
            rating = int(request.POST['rating'])

            user = User2.objects.get(vk_id=vk_user_id)
            ct = CompletedTask.objects.get(id=completed_task_id)

            # Если голосование за самого себя - возвращаем отмену.
            if (user == ct.user):
                return JsonResponse({'success': False, 'current_rating': ct.get_rating(), 'error': 'Голосование за самого себя. Отмена.'})

            # Если пользователь уже голосовал за это выполненное задание - возвращаем отмену.
            count_user_votes = RatingCompletedTask.objects.filter(user=user, completed_task=ct).count()
            if (count_user_votes > 0):
            	return JsonResponse({'success': False, 'current_rating': ct.get_rating(), 'error': 'Вы уже голосовали за это выполненное задание. Отмена.'})

            rct = RatingCompletedTask(user=user, completed_task=ct, vote=rating)
            rct.save()
            # return HttpResponse('Ваш голос был учтён, спасибо.')
            return JsonResponse({'success': True, 'current_rating': ct.get_rating()})
        except:
            return JsonResponse({'success': False, 'error': 'some error'})
    return JsonResponse({'success': False})

@csrf_exempt
def user_complete_task(request):
    # Ф-я принимает id пользователя, id задачи-родителя, комментарий и картинку. Записыва это в БД.
    if (request.method == 'POST'):
        try:
            vk_user_id = int(request.POST['vk_user_id'])
            origin_task_id = int(request.POST['origin_task_id'])
            user_comment = request.POST['user_comment']
            image = request.POST['image']

            user = User2.objects.get(vk_id=vk_user_id)
            origin_task = Task.objects.get(id=origin_task_id)

            ct = CompletedTask(user_comment=user_comment, user=user, image=image, task_origin=origin_task)
            ct.save()
            # return HttpResponse('Ваше задание было добавлено на сервер.')
            return JsonResponse({'success': True})
        except:
            return JsonResponse({'success': False})
    return JsonResponse({'success': False})



# Страницы url.
@csrf_exempt
def index(request):
    # Показываем страницу с тестами api.
    return HttpResponse(render_to_string('test_api.html'))


