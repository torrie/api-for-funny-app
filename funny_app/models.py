from django.db import models
import datetime

class User2(models.Model):
    """
    Модель пользователя.
    """
    vk_id = models.IntegerField(verbose_name='id пользователя в vk')
    name = models.CharField(max_length=250, verbose_name='Имя пользователя')
    image = models.URLField(max_length=250, verbose_name='Картинка')

    class Meta:
        # ordering = ["id"]
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def __str__(self):
        return self.name + ' (vk-id: ' + str(self.vk_id) + ')'

    def get_rating(self):
        # Ф-я динамически подсчитывает рейтинг юзера:
        # 1) сумма оценок выполненных заданий от других польз-й;
        # 2) сумма очков выполненных заданий.
        sum_c_tasks_votes = sum_c_tasks_scores = 0

        # 1) Загружаем все выполненные задания и суммируем их рейтинг.
        completed_tasks = CompletedTask.objects.filter(user=self)
        for ct in completed_tasks:
            sum_c_tasks_votes += ct.get_rating()
            sum_c_tasks_scores += ct.task_origin.score_to_complete
        return (sum_c_tasks_votes + sum_c_tasks_scores)

class Task(models.Model):
    """
    Модель заданий.
    Важно понять: это само задание, задача, которую будут делать пользователи.
    К ней будет крепиться много решений. Решение - это то что сделал пользователь.
    """
    title = models.CharField(max_length=250, blank=False, verbose_name='Название')
    descr_short = models.TextField(blank=False, verbose_name='Краткое описание')
    descr_full = models.TextField(blank=False, verbose_name='Длинное описание')
    image = models.CharField(max_length=250, blank=False, verbose_name='Картинка')
    score_to_complete = models.IntegerField(blank=False, verbose_name='Очки за выполнение')

    class Meta:
        # ordering = ["id"]
        verbose_name = "Задание"
        verbose_name_plural = "Задания"

    def __str__(self):
        return 'Базовая идея: ' + self.title

class CompletedTask(models.Model):
    """
    Модель выполненного задания.
    """
    user_comment = models.TextField(blank=True, verbose_name='Текст, введённый пользователем')
    user = models.ForeignKey(User2, blank=False, verbose_name='Пользователь, выполнивший это задание')
    image = models.URLField(max_length=250, blank=False, verbose_name='Картинка')
    task_origin = models.ForeignKey(Task, blank=False, verbose_name='Ссылка на оригинальное задание')

    class Meta:
        # ordering = ["id"]
        verbose_name = "Выполненное задание"
        verbose_name_plural = "Выполненные задания"

    def __str__(self):
        return self.task_origin.title + '; выполнил ' + str(self.user)

    def get_rating(self):
        # Ф-я подсчитывает райтинг выполненного задания.
        sum = 0
        # Получаем все оценки этого задания.
        r = RatingCompletedTask.objects.filter(completed_task=self)
        for i in r:
            sum += i.vote
        return sum

class RatingCompletedTask(models.Model):
    user = models.ForeignKey(User2, blank=False, verbose_name='Пользователь, оценивший это задание')
    vote = models.IntegerField(blank=True, verbose_name='Очки по голосованию')
    date = models.DateTimeField(default=datetime.datetime.now(), blank=False, verbose_name='Дата оценки')
    completed_task = models.ForeignKey(CompletedTask, blank=False, verbose_name='Ссылка на связанную выполненную задачу')

    class Meta:
        # ordering = ["id"]
        verbose_name = "Рейтинг выполненных заданий"
        verbose_name_plural = "Рейтингов выполненных заданий"

    def __str__(self):
        return 'Рейтинг: ' + str(self.vote) + ' от ' + str(self.user) + ' для задачи ' + str(self.completed_task)