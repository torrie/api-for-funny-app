from django.contrib import admin
from funny_app.models import *

class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'descr_short', 'descr_full', 'score_to_complete',)
admin.site.register(Task, TaskAdmin)

class UserAdmin(admin.ModelAdmin):
    list_display = ('vk_id', 'name',)
admin.site.register(User2, UserAdmin)

class CompletedTaskAdmin(admin.ModelAdmin):
    list_display = ('user_comment', 'user', 'task_origin',)
admin.site.register(CompletedTask, CompletedTaskAdmin)

class RatingCompletedTaskAdmin(admin.ModelAdmin):
    list_display = ('vote', 'completed_task', 'user', 'date',)
admin.site.register(RatingCompletedTask, RatingCompletedTaskAdmin)