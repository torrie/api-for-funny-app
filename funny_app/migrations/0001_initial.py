# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CompletedTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_comment', models.TextField(verbose_name='Текст, введённый пользователем', blank=True)),
                ('image', models.URLField(verbose_name='Картинка', max_length=250)),
            ],
            options={
                'verbose_name': 'Выполненное задание',
                'verbose_name_plural': 'Выполненные задания',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RatingCompletedTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vote', models.IntegerField(verbose_name='Очки по голосованию', blank=True)),
                ('date', models.DateTimeField(verbose_name='Дата оценки', default=datetime.datetime(2015, 5, 24, 6, 33, 33, 545824))),
                ('completed_task', models.ForeignKey(verbose_name='Ссылка на связанную выполненную задачу', to='funny_app.CompletedTask')),
            ],
            options={
                'verbose_name': 'Рейтинг выполненных заданий',
                'verbose_name_plural': 'Рейтингов выполненных заданий',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(verbose_name='Название', max_length=250)),
                ('descr_short', models.TextField(verbose_name='Краткое описание')),
                ('descr_full', models.TextField(verbose_name='Длинное описание')),
                ('image', models.CharField(verbose_name='Картинка', max_length=250)),
                ('score_to_complete', models.IntegerField(verbose_name='Очки за выполнение')),
            ],
            options={
                'verbose_name': 'Задание',
                'verbose_name_plural': 'Задания',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='User2',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vk_id', models.IntegerField(verbose_name='id пользователя в vk')),
                ('name', models.CharField(verbose_name='Имя пользователя', max_length=250)),
                ('image', models.URLField(verbose_name='Картинка', max_length=250)),
            ],
            options={
                'verbose_name': 'Пользователь',
                'verbose_name_plural': 'Пользователи',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='ratingcompletedtask',
            name='user',
            field=models.ForeignKey(verbose_name='Пользователь, оценивший это задание', to='funny_app.User2'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='completedtask',
            name='task_origin',
            field=models.ForeignKey(verbose_name='Ссылка на оригинальное задание', to='funny_app.Task'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='completedtask',
            name='user',
            field=models.ForeignKey(verbose_name='Пользователь, выполнивший это задание', to='funny_app.User2'),
            preserve_default=True,
        ),
    ]
